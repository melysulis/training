package com.ist.training.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "lokasi")
@Data
public class LokasiPKL {

    @Id
    @GeneratedValue
    private Long id_lokasi;
    private String nama_instansi;
    //private Long id_jenis;

    @OneToMany(mappedBy = "lokasi")
    @JsonBackReference
    private List<Mahasiswa> mahasiswa;

    //@ManyToOne
    //@JoinColumn(name = "id_jenis")
    //@JsonBackReference
    //private JenisLokasi jenis_lokasi;
}
