package com.ist.training.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "jenis_lokasi")
@Data
public class JenisLokasi {

    @Id
    @GeneratedValue
    private Long id_jenis;
    private String jenis;

    //@OneToMany(mappedBy = "jenis_lokasi")
    //@JsonBackReference
    //private List<LokasiPKL> lokasi;
}
