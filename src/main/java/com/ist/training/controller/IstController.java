package com.ist.training.controller;

import com.ist.training.entity.Mahasiswa;
import com.ist.training.entity.User;
import com.ist.training.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Slf4j
public class IstController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/hello")
    public String helloworld(){
        return "Hello World";
    }

    @GetMapping("/getAllMhs")
    public ResponseEntity<List<Mahasiswa>> getAllMhs(){
        List<Mahasiswa> result = userRepository.findAll();
        if(result == null || result.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @PostMapping("/saveMhs")
    public ResponseEntity<Mahasiswa> saveMhs(@RequestBody Mahasiswa mhs){
        try{
            Mahasiswa u = userRepository.save(mhs);
            return new ResponseEntity<>(u, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error saving {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   /*@PostMapping("/saveMahasiswa")
    public ResponseEntity<Mahasiswa> saveMahasiswa(@RequestBody Mahasiswa mhs){
        Mahasiswa m = userRepository.save(mhs);
        return new ResponseEntity<>(m,HttpStatus.CREATED);
    }*/

    @DeleteMapping("/deleteMhs")
    public ResponseEntity<String> deleteMhs(@RequestParam String id_mhs){
        try{
            userRepository.deleteById(Long.valueOf(id_mhs));
            return new ResponseEntity<>("Deleted", HttpStatus.OK );
        }catch (Exception e){
            log.error("Error deleting {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/updateMhs")
    public Mahasiswa updateMhs(@RequestBody Mahasiswa mhs){
        Mahasiswa result = userRepository.findById(mhs.getId_mhs()).orElse(mhs);
        result.setNim(mhs.getNim());
        result.setNama(mhs.getNama());
        return userRepository.save(result);
    }

}
