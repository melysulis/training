package com.ist.training.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "mahasiswa")
@Data
public class Mahasiswa {

    @Id
    @GeneratedValue
    private Long id_mhs;
    private String nim;
    private String nama;
    //private Long id_lokasi;

    @ManyToOne
    @JoinColumn(name = "id_lokasi")
    //@JsonBackReference
    private LokasiPKL lokasi;


}
